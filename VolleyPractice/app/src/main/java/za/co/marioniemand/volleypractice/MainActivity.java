package za.co.marioniemand.volleypractice;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void apiGetRequest(View v) throws JSONException, InterruptedException{
        final TextView textViewUsername = (TextView) findViewById(R.id.username);
        final TextView textViewEmail = (TextView) findViewById(R.id.email);
        final TextInputEditText textEdit = (TextInputEditText) findViewById(R.id.input);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://6b84-192-143-110-211.ngrok.io/users";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                //System.out.println(response.toString());

                JSONObject selectedUser = null;
                try {
                    selectedUser = returnSearch(response, (String) textEdit.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    textViewUsername.setText("Response: " + selectedUser.getString("name"));
                    textViewEmail.setText("Response: " + selectedUser.getString("email"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                textViewUsername.setText("error");
            }
        });

        queue.add(jsonArrayRequest);
    }

    public JSONObject returnSearch(JSONArray array, String searchValue) throws JSONException {
        System.out.println(searchValue);
        JSONObject result = null;
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject obj = null;
                obj = array.getJSONObject(i);
                if(obj.getString("name").equals(searchValue))
                {
                    System.out.println(obj);
                    result = obj;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }
}